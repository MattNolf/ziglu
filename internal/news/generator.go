package news

import uuid "github.com/kevinburke/go.uuid"

type UUIDGenerator struct{}

func (g UUIDGenerator) GenerateID() string {
	return uuid.NewV4().String()
}

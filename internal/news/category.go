package news

// Category of news article indicating the content type
type Category string

const (
	// General content not otherewise classfied
	General Category = "General"
	// Technology related content
	Technology Category = "Technology"
)

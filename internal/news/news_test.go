package news_test

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/mattnolf/ziglu/internal/news"
	mock_news "github.com/mattnolf/ziglu/mock/news"
)

func Test_New(t *testing.T) {
	t.Run("return aggregator", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mf := mock_news.NewMockNewsFeed(ctrl)
		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		aggregator, err := news.New([]news.NewsFeed{mf}, mr, mg)
		require.NoError(t, err)

		assert.NotNil(t, aggregator)
	})
	t.Run("return error given invalid newsfeeds", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		_, err := news.New([]news.NewsFeed{nil}, mr, mg)

		assert.Error(t, err)

	})
	t.Run("return error given empty newsfeeds", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		_, err := news.New(nil, mr, mg)

		assert.Error(t, err)
	})
	t.Run("return error given invalid store", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mf := mock_news.NewMockNewsFeed(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		_, err := news.New([]news.NewsFeed{mf}, nil, mg)

		assert.Error(t, err)
	})
}

func Test_AggregatorGetArticles(t *testing.T) {
	ctx := context.Background()
	accountID := "some-account-id"
	articleID := "some-item-id"
	someOtherArticleID := "some-other-item-id"
	t.Run("return articles", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mf := mock_news.NewMockNewsFeed(ctrl)
		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		aggregator, err := news.New([]news.NewsFeed{mf}, mr, mg)
		require.NoError(t, err)

		expected := []*news.Item{
			{
				ID: articleID,
			},
		}
		mf.EXPECT().Name().Return("some-feed")
		mg.EXPECT().GenerateID().Return(articleID)
		mf.EXPECT().Feed(context.Background()).Return(expected, nil)
		mr.EXPECT().IsRead(context.Background(), accountID, articleID).Return(false, nil)

		someOpts := news.QueryOptions{}
		actual, err := aggregator.GetArticles(ctx, accountID, &someOpts)

		require.NoError(t, err)

		assert.Equal(t, expected, actual)
	})
	t.Run("return articles and filter out read ones", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mf := mock_news.NewMockNewsFeed(ctrl)
		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		aggregator, err := news.New([]news.NewsFeed{mf}, mr, mg)
		require.NoError(t, err)

		items := []*news.Item{
			{
				ID: articleID,
			},
			{
				ID: someOtherArticleID,
			},
		}
		mf.EXPECT().Name().Return("some-feed")
		mf.EXPECT().Feed(context.Background()).Return(items, nil)
		mg.EXPECT().GenerateID().Return(articleID)
		mr.EXPECT().IsRead(context.Background(), accountID, articleID).Return(true, nil)
		mg.EXPECT().GenerateID().Return(someOtherArticleID)
		mr.EXPECT().IsRead(context.Background(), accountID, someOtherArticleID).Return(false, nil)

		someOpts := news.QueryOptions{}
		actual, err := aggregator.GetArticles(ctx, accountID, &someOpts)

		require.NoError(t, err)

		expected := []*news.Item{
			{
				ID: someOtherArticleID,
			},
		}
		assert.Equal(t, expected, actual)

	})
	t.Run("return error given unknown provider", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mf := mock_news.NewMockNewsFeed(ctrl)
		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		aggregator, err := news.New([]news.NewsFeed{mf}, mr, mg)
		require.NoError(t, err)

		mf.EXPECT().Name().Return("some-feed")

		someOpts := news.QueryOptions{
			Providers: []string{"unknown"},
		}
		_, err = aggregator.GetArticles(ctx, accountID, &someOpts)

		assert.Error(t, err)
	})
}

func Test_AggregatorMarkRead(t *testing.T) {
	ctx := context.Background()
	accountID := "some-account-id"
	articleID := "some-item-id"

	t.Run("return nil and mark read", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mf := mock_news.NewMockNewsFeed(ctrl)
		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		aggregator, err := news.New([]news.NewsFeed{mf}, mr, mg)
		require.NoError(t, err)

		mr.EXPECT().MarkRead(ctx, accountID, articleID).Return(nil)

		err = aggregator.MarkRead(ctx, accountID, articleID)

		assert.NoError(t, err)
	})
	t.Run("return error given invalid accountID", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mf := mock_news.NewMockNewsFeed(ctrl)
		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		aggregator, err := news.New([]news.NewsFeed{mf}, mr, mg)
		require.NoError(t, err)

		err = aggregator.MarkRead(ctx, "", articleID)

		assert.Error(t, err)
	})
	t.Run("return error given invalid articleID", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mf := mock_news.NewMockNewsFeed(ctrl)
		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		aggregator, err := news.New([]news.NewsFeed{mf}, mr, mg)
		require.NoError(t, err)

		err = aggregator.MarkRead(ctx, accountID, "")

		assert.Error(t, err)
	})
	t.Run("return error given error from store", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mf := mock_news.NewMockNewsFeed(ctrl)
		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		aggregator, err := news.New([]news.NewsFeed{mf}, mr, mg)
		require.NoError(t, err)

		mr.EXPECT().MarkRead(ctx, accountID, articleID).Return(errors.New("some-error"))

		err = aggregator.MarkRead(ctx, accountID, articleID)

		assert.Error(t, err)
	})
}

func Test_AggregatorGenerateShareLink(t *testing.T) {
	ctx := context.Background()
	accountID := "some-account-id"
	articleID := "some-item-id"

	t.Run("return nil and mark read", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mf := mock_news.NewMockNewsFeed(ctrl)
		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		aggregator, err := news.New([]news.NewsFeed{mf}, mr, mg)
		require.NoError(t, err)

		link, err := aggregator.GenerateShareLink(ctx, accountID, articleID)

		assert.NoError(t, err)
		assert.Contains(t, link, articleID)
	})
	t.Run("return error given invalid accountID", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mf := mock_news.NewMockNewsFeed(ctrl)
		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		aggregator, err := news.New([]news.NewsFeed{mf}, mr, mg)
		require.NoError(t, err)

		_, err = aggregator.GenerateShareLink(ctx, "", articleID)

		assert.Error(t, err)
	})
	t.Run("return error given invalid articleID", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mf := mock_news.NewMockNewsFeed(ctrl)
		mr := mock_news.NewMockStore(ctrl)
		mg := mock_news.NewMockGenerator(ctrl)

		aggregator, err := news.New([]news.NewsFeed{mf}, mr, mg)
		require.NoError(t, err)

		_, err = aggregator.GenerateShareLink(ctx, accountID, "")

		assert.Error(t, err)
	})
}

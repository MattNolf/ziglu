package news

import (
	"context"
	"errors"
	"fmt"
	"time"
)

// Item is a normalized view of a news article, to be used internally by the news app & API.
type Item struct {
	ID          string    `json:"id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	URL         string    `json:"url"`
	ImageURL    string    `json:"imageURL"`
	Category    Category  `json:"category"`
	Provider    string    `json:"provider"`
	Timestamp   time.Time `json:"timestamp"`
}

// NewsFeed provides functionality to get a feed of news items.
type NewsFeed interface {
	// Name returns the news feed name.
	Name() string
	// Feed returns a feed of news items.
	Feed(ctx context.Context) ([]*Item, error)
}

// Store provides functionality to search, and mark news items as read by an account.
type Store interface {
	// IsRead returns true if the given articleID has been read by the specified accountID.
	IsRead(ctx context.Context, accountID string, articleID string) (bool, error)
	// MarkRead persists that the given articleID has been read by the specified accountID.
	MarkRead(ctx context.Context, accountID string, articleID string) error
}

// Generator provides functionality generate random values
type Generator interface {
	// Generate a random string
	GenerateID() string
}

type Aggregator struct {
	feeds     []NewsFeed
	store     Store
	generator Generator
	idMap     map[string]string
}

func New(f []NewsFeed, mr Store, generator Generator) (*Aggregator, error) {
	if len(f) == 0 {
		return nil, errors.New("empty_newsfeed_slice")
	}
	for idx, _ := range f {
		if f[idx] == nil {
			return nil, errors.New("invalid_newsfeed")
		}
	}
	if mr == nil {
		return nil, errors.New("invalid_store")
	}
	if generator == nil {
		return nil, errors.New("invalid_generator")
	}

	return &Aggregator{
		feeds:     f,
		store:     mr,
		generator: generator,
		idMap:     make(map[string]string),
	}, nil
}

type QueryOptions struct {
	Categories []string
	Providers  []string
	ShowRead   bool
}

func (a *Aggregator) GetArticles(ctx context.Context, accountID string, opts *QueryOptions) ([]*Item, error) {
	if opts == nil {
		return nil, errors.New("invalid_options")
	}

	if err := a.validateQueryOptions(opts); err != nil {
		return nil, fmt.Errorf("invalid_options: %w", err)
	}
	var items []*Item

	for _, feed := range a.feeds {
		if !contains(feed.Name(), opts.Categories) {
			continue
		}
		is, err := feed.Feed(ctx)
		if err != nil {
			return nil, fmt.Errorf("get_feed: %s", feed.Name())
		}

		for _, item := range is {
			id, ok := a.idMap[item.ID]
			if !ok {
				externalID := a.generator.GenerateID()
				a.idMap[item.ID] = externalID
				id = externalID
			} else {
				item.ID = id
			}

			ok, err := a.store.IsRead(ctx, accountID, id)
			if err != nil {
				return nil, fmt.Errorf("check_is_read: %w", err)
			}
			if !contains(string(item.Category), opts.Categories) {
				continue
			}
			if opts.ShowRead || !ok {
				items = append(items, item)
			}
		}
	}

	return items, nil
}

func (a *Aggregator) MarkRead(ctx context.Context, accountID string, articleID string) error {
	if accountID == "" {
		return fmt.Errorf("empty_account_id")
	}
	if articleID == "" {
		return fmt.Errorf("empty_article_id")
	}

	err := a.store.MarkRead(ctx, accountID, articleID)
	if err != nil {
		return fmt.Errorf("mark_read: %w", err)
	}
	return nil
}

func (a *Aggregator) GenerateShareLink(ctx context.Context, accountID string, articleID string) (string, error) {
	if accountID == "" {
		return "", fmt.Errorf("empty_account_id")
	}
	if articleID == "" {
		return "", fmt.Errorf("empty_article_id")
	}

	// TODO (Matthew Nolf): Create shareable link
	return fmt.Sprintf("https://super-sharable-link.com/%s", articleID), nil
}

func (a *Aggregator) validateQueryOptions(qo *QueryOptions) error {
	for _, provider := range qo.Providers {
		found := false
		for _, feed := range a.feeds {
			if feed.Name() == provider {
				found = true
			}
		}
		if !found {
			return fmt.Errorf("unsupported_provider: %s", provider)
		}
	}
	return nil
}

func contains(f string, fs []string) bool {
	if len(fs) == 0 {
		return true
	}
	for _, val := range fs {
		if val == f {
			return true
		}
	}
	return false
}

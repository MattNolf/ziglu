package skytech

import (
	"context"
	"encoding/xml"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/mattnolf/ziglu/internal/news"
)

type client struct {
	url string
	c   *http.Client
}

// NewClient returns a new sky technology RSS feed client.
func NewClient(address string, c *http.Client) (*client, error) {
	_, err := url.Parse(address)
	if err != nil {
		return nil, fmt.Errorf("parse_url: %w", err)
	}
	if c == nil {
		return nil, fmt.Errorf("nil_http_client: %w", err)
	}
	return &client{
		url: address,
		c:   c,
	}, nil
}

type channel struct {
	Title       string `xml:"title"`
	Link        string `xml:"link"`
	Description string `xml:"description"`
	Language    string `xml:"language"`
	Item        []item `xml:"item"`
}

type item struct {
	ID          string `xml:"guid"`
	Title       string `xml:"title"`
	Description string `xml:"description"`
	Link        string `xml:"link"`
	Date        string `xml:"pubDate"`
}

func (c *client) Name() string { return "Sky Technology News" }

func (c *client) Feed(ctx context.Context) ([]*news.Item, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, c.url, nil)
	if err != nil {
		return nil, fmt.Errorf("create_request: %w", err)
	}

	resp, err := c.c.Do(req)
	if err != nil {
		return nil, fmt.Errorf("do_request: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("received_bad_status: %s", resp.Status)
	}

	if ct := resp.Header.Get("Content-Type"); ct != "text/xml; charset=utf-8" {
		return nil, fmt.Errorf("received_bad_content_type: %s", ct)
	}

	var is struct {
		Channel channel `xml:"channel"`
	}

	err = xml.NewDecoder(resp.Body).Decode(&is)
	if err != nil {
		return nil, fmt.Errorf("unmarshal_body: %w", err)
	}

	if len(is.Channel.Item) < 1 {
		return nil, fmt.Errorf("received_no_items")
	}

	i := make([]*news.Item, len(is.Channel.Item))
	for idx := range is.Channel.Item {
		i[idx] = c.toNewsModel(is.Channel.Item[idx])
	}

	return i, nil
}

func (c *client) toNewsModel(i item) *news.Item {
	t, err := time.Parse(time.RFC1123, i.Date)
	if err != nil {
		t = time.Now()
	}

	return &news.Item{
		ID:          i.ID,
		Title:       i.Title,
		Description: i.Description,
		URL:         i.Link,
		Category:    news.Technology, // AI ;)
		Provider:    c.Name(),
		Timestamp:   t,
	}
}

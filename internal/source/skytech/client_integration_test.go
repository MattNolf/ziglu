//+build integration

package skytech_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/mattnolf/ziglu/internal/source/skytech"
)

func Test_Feed_Integration(t *testing.T) {
	client, err := skytech.NewClient("http://feeds.skynews.com/feeds/rss/technology.xml", http.DefaultClient)
	require.NoError(t, err)

	items, err := client.Feed(context.Background())
	assert.NoError(t, err)

	assert.NotZero(t, items)
}

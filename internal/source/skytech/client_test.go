package skytech_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/mattnolf/ziglu/internal/source/skytech"
)

func Test_New(t *testing.T) {
	t.Run("return client", func(t *testing.T) {
		client, err := skytech.NewClient("http://feeds.skynews.com/feeds/rss/technology.xml", http.DefaultClient)
		require.NoError(t, err)

		assert.NotNil(t, client)
	})
	t.Run("return error given invalid address", func(t *testing.T) {
		_, err := skytech.NewClient("bla%^&*", http.DefaultClient)
		assert.Error(t, err)
	})
	t.Run("return error given nil http client", func(t *testing.T) {
		_, err := skytech.NewClient("http://feeds.skynews.com/feeds/rss/technology.xml", nil)
		assert.Error(t, err)
	})
}

func Test_Name(t *testing.T) {
	t.Run("return name", func(t *testing.T) {
		client, err := skytech.NewClient("http://feeds.skynews.com/feeds/rss/technology.xml", http.DefaultClient)
		require.NoError(t, err)

		name := client.Name()

		assert.Equal(t, "Sky Technology News", name)
	})
}
func Test_Feed(t *testing.T) {
	ctx := context.Background()
	t.Run("return items and request feed from sky", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			assert.Equal(t, http.MethodGet, r.Method)
			assert.Equal(t, http.NoBody, r.Body)

			rw.Header().Set("Content-Type", "text/xml; charset=utf-8")
			rw.Write(sampleBody(t))
		}))

		client, err := skytech.NewClient(srv.URL, srv.Client())
		require.NoError(t, err)

		is, err := client.Feed(ctx)
		assert.NoError(t, err)

		assert.Len(t, is, 2)

		assert.Equal(t, `Instagram to allow teens to block messages from adults`, is[0].Title)
		// TODO (Matthew Nolf): Finish assertions
	})
	t.Run("return error given invalid response code", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			assert.Equal(t, http.MethodGet, r.Method)
			assert.Equal(t, http.NoBody, r.Body)

			rw.Header().Set("Content-Type", "text/xml; charset=utf-8")
			rw.WriteHeader(http.StatusNotFound)
		}))

		client, err := skytech.NewClient(srv.URL, srv.Client())
		require.NoError(t, err)

		_, err = client.Feed(ctx)
		assert.Error(t, err)

		assert.Contains(t, err.Error(), "received_bad_status: 404")
	})
	t.Run("return error given invalid content type", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			assert.Equal(t, http.MethodGet, r.Method)
			assert.Equal(t, http.NoBody, r.Body)

			rw.Header().Set("Content-Type", "application/json")
			rw.Write(sampleBody(t))
		}))

		client, err := skytech.NewClient(srv.URL, srv.Client())
		require.NoError(t, err)

		_, err = client.Feed(ctx)
		assert.Error(t, err)

		assert.Contains(t, err.Error(), "received_bad_content_type: application/json")
	})
	t.Run("return error given invalid response body", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			assert.Equal(t, http.MethodGet, r.Method)
			assert.Equal(t, http.NoBody, r.Body)

			rw.Header().Set("Content-Type", "text/xml; charset=utf-8")
			rw.Write([]byte(`bad-response`))
		}))

		client, err := skytech.NewClient(srv.URL, srv.Client())
		require.NoError(t, err)

		_, err = client.Feed(ctx)
		assert.Error(t, err)

		assert.Contains(t, err.Error(), "unmarshal_body")
	})
	t.Run("return error given empty body", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			assert.Equal(t, http.MethodGet, r.Method)
			assert.Equal(t, http.NoBody, r.Body)

			rw.Header().Set("Content-Type", "text/xml; charset=utf-8")
			rw.Write(emptyBody(t))
		}))

		client, err := skytech.NewClient(srv.URL, srv.Client())
		require.NoError(t, err)

		_, err = client.Feed(ctx)
		assert.Error(t, err)

		assert.Contains(t, err.Error(), "received_no_items")
	})
}

// emptyBody is a sample response with news items
func sampleBody(t *testing.T) []byte {
	t.Helper()

	return []byte(`
	<?xml version="1.0" encoding="UTF-8"?>
		<rss
		xmlns:atom="http://www.w3.org/2005/Atom"
		xmlns:media="http://search.yahoo.com/mrss/" version="2.0">
		<channel>
			<atom:link href="http://feeds.skynews.com/feeds/rss/technology.xml" rel="self" type="application/rss+xml"/>
			<title>Tech News - Latest Technology and Gadget News | Sky News</title>
			<link>http://news.sky.com/technology</link>
			<image>
			<title>Tech News - Latest Technology and Gadget News | Sky News</title>
			<url>http://feeds.skynews.com/images/web/logo/skynews_rss.png</url>
			<link>http://news.sky.com/technology</link>
			</image>
			<description>Sky News technology provides you with all the latest tech and gadget news, game reviews, Internet and web news across the globe. Visit us today.</description>
			<language>en-GB</language>
			<copyright>Copyright 2016, BSKYB. All Rights Reserved.</copyright>
			<lastBuildDate>Tue, 16 Mar 2021 15:24:00 +0000</lastBuildDate>
			<category>Sky News</category>
			<ttl>1</ttl>
			
			<item>
			<title>Instagram to allow teens to block messages from adults</title>
			<link>http://news.sky.com/story/instagram-to-allow-teens-to-block-messages-from-adults-12247875</link>
			<description>Instagram is introducing new anti-grooming safety measures that will prevent adult users from sending direct messages to teenagers unless they follow them.</description>
			<pubDate>Tue, 16 Mar 2021 15:24:00 +0000</pubDate>
			<guid>http://news.sky.com/story/instagram-to-allow-teens-to-block-messages-from-adults-12247875</guid>
			<enclosure url="http://e3.365dm.com/21/03/70x70/skynews-instagram-logo-phone_5307208.jpg?20210316152841" length="0" type="image/jpeg"/>
			<media:description type="html">The logo of the App instagram is on the display of an iPhone 6, Germany, city of Osterode, 17. May 2016. Photo by: Frank May/picture-alliance/dpa/AP Images
			</media:description>
			<media:thumbnail url="http://e3.365dm.com/21/03/70x70/skynews-instagram-logo-phone_5307208.jpg?20210316152841" width="70" height="70"/>
			<media:content type="image/jpeg" url="http://e3.365dm.com/21/03/70x70/skynews-instagram-logo-phone_5307208.jpg?20210316152841"/>
			</item>
			
			<item>
			<title>Lightning strikes may have sparked life on Earth, scientists claim</title>
			<link>http://news.sky.com/story/lightning-strikes-may-have-sparked-life-on-earth-according-to-new-research-in-uk-and-us-12247785</link>
			<description>While many questions remain on the origins of life on Earth, scientists in the US and UK now believe that lightning strikes may have played an essential role.</description>
			<pubDate>Tue, 16 Mar 2021 14:19:00 +0000</pubDate>
			<guid>http://news.sky.com/story/lightning-strikes-may-have-sparked-life-on-earth-according-to-new-research-in-uk-and-us-12247785</guid>
			<enclosure url="http://e3.365dm.com/21/03/70x70/skynews-lightning-cloud_5307125.jpg?20210316142525" length="0" type="image/jpeg"/>
			<media:description type="html">Lightning strikes the Mediterranean Sea near Hadera, Israel, Thursday, Dec. 6, 2018. (AP Photo/Ariel Schalit)


			</media:description>
			<media:thumbnail url="http://e3.365dm.com/21/03/70x70/skynews-lightning-cloud_5307125.jpg?20210316142525" width="70" height="70"/>
			<media:content type="image/jpeg" url="http://e3.365dm.com/21/03/70x70/skynews-lightning-cloud_5307125.jpg?20210316142525"/>
			</item>
		</channel>
		</rss>
`)
}

// emptyBody is a sample response with no news items
func emptyBody(t *testing.T) []byte {
	t.Helper()

	return []byte(`
	<?xml version="1.0" encoding="UTF-8"?>
		<rss
		xmlns:atom="http://www.w3.org/2005/Atom"
		xmlns:media="http://search.yahoo.com/mrss/" version="2.0">
		<channel>
			<atom:link href="http://feeds.skynews.com/feeds/rss/technology.xml" rel="self" type="application/rss+xml"/>
			<title>Tech News - Latest Technology and Gadget News | Sky News</title>
			<link>http://news.sky.com/technology</link>
			<image>
			<title>Tech News - Latest Technology and Gadget News | Sky News</title>
			<url>http://feeds.skynews.com/images/web/logo/skynews_rss.png</url>
			<link>http://news.sky.com/technology</link>
			</image>
			<description>Sky News technology provides you with all the latest tech and gadget news, game reviews, Internet and web news across the globe. Visit us today.</description>
			<language>en-GB</language>
			<copyright>Copyright 2016, BSKYB. All Rights Reserved.</copyright>
			<lastBuildDate>Tue, 16 Mar 2021 15:24:00 +0000</lastBuildDate>
			<category>Sky News</category>
			<ttl>1</ttl>
			
		</channel>
		</rss>
`)
}

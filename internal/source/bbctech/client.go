package bbctech

import (
	"context"
	"errors"
	"net/http"

	"github.com/mattnolf/ziglu/internal/news"
)

type client struct {
	url string
	c   *http.Client
}

// NewClient returns a new sky technology RSS feed client.
func NewClient(address string, c *http.Client) (*client, error) {
	// TODO (Matthew Nolf): Implement bbc RSS feed...
	return nil, errors.New("not implemented")
}

func (c *client) Name() string { return "BBC Technology News" }

func (c *client) Feed(ctx context.Context) ([]*news.Item, error) {
	return nil, nil
}

package memorystore_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/mattnolf/ziglu/internal/database/memorystore"
)

func Test_IsRead(t *testing.T) {
	ctx := context.Background()
	t.Run("return true if article is read", func(t *testing.T) {
		c := memorystore.NewClient()

		someAccountID := "some-accountID"
		someArticleID := "some-articleID"

		err := c.MarkRead(ctx, someAccountID, someArticleID)
		require.NoError(t, err)

		read, err := c.IsRead(ctx, someAccountID, someArticleID)
		require.NoError(t, err)

		assert.True(t, read)
	})
	t.Run("return false if article is not", func(t *testing.T) {
		c := memorystore.NewClient()

		someAccountID := "some-accountID"
		someArticleID := "some-articleID"
		someOtherArticleID := "some-other-articleID"

		err := c.MarkRead(ctx, someAccountID, someArticleID)
		require.NoError(t, err)

		read, err := c.IsRead(ctx, someAccountID, someOtherArticleID)
		require.NoError(t, err)

		assert.False(t, read)
	})
	t.Run("return false if user is not found", func(t *testing.T) {
		c := memorystore.NewClient()

		someAccountID := "some-accountID"
		someArticleID := "some-articleID"

		read, err := c.IsRead(ctx, someAccountID, someArticleID)
		require.NoError(t, err)

		assert.False(t, read)
	})
}

func Test_MarkRead(t *testing.T) {
	ctx := context.Background()

	t.Run("return nil and mark as read", func(t *testing.T) {
		c := memorystore.NewClient()

		someAccountID := "some-accountID"
		someArticleID := "some-articleID"

		err := c.MarkRead(ctx, someAccountID, someArticleID)
		require.NoError(t, err)

		read, err := c.IsRead(ctx, someAccountID, someArticleID)
		require.NoError(t, err)

		assert.True(t, read)
	})
	t.Run("return nil if already mark as read", func(t *testing.T) {
		c := memorystore.NewClient()

		someAccountID := "some-accountID"
		someArticleID := "some-articleID"

		err := c.MarkRead(ctx, someAccountID, someArticleID)
		require.NoError(t, err)

		read, err := c.IsRead(ctx, someAccountID, someArticleID)
		require.NoError(t, err)

		err = c.MarkRead(ctx, someAccountID, someArticleID)
		assert.NoError(t, err)

		assert.True(t, read)
	})
}

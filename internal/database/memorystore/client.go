package memorystore

import (
	"context"
	"sync"
)

type account string
type item string

type client struct {
	sync.Mutex
	readRecepits map[account][]item
}

func NewClient() *client {
	return &client{
		readRecepits: make(map[account][]item),
	}
}

func (c *client) IsRead(ctx context.Context, accountID string, articleID string) (bool, error) {
	c.Lock()
	defer c.Unlock()

	if items, ok := c.readRecepits[account(accountID)]; ok {
		for _, val := range items {
			if val == item(articleID) {
				return true, nil
			}
		}
	}
	return false, nil
}
func (c *client) MarkRead(ctx context.Context, accountID string, articleID string) error {
	c.Lock()
	defer c.Unlock()

	if _, ok := c.readRecepits[account(accountID)]; !ok {
		c.readRecepits[account(accountID)] = []item{}
	}

	c.readRecepits[account(accountID)] = append(c.readRecepits[account(accountID)], item(articleID))

	return nil
}

package transporthttp

import (
	"net/http"

	"github.com/gorilla/mux"
)

func ListenAndServe(address string, h *Handler) error {
	r := mux.NewRouter()
	h.ApplyRoutes(r)

	if err := http.ListenAndServe(address, r); err != nil {
		return err
	}
	return nil

}

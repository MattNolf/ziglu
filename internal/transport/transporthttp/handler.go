package transporthttp

import (
	"context"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"

	"github.com/mattnolf/ziglu/internal/news"
)

type Aggregator interface {
	GetArticles(ctx context.Context, accountID string, opts *news.QueryOptions) ([]*news.Item, error)
	MarkRead(ctx context.Context, accountID string, articleID string) error
	GenerateShareLink(ctx context.Context, accountID string, articleID string) (string, error)
}

type Handler struct {
	aggregator Aggregator
}

func NewHandler(a Aggregator) (*Handler, error) {
	if a == nil {
		return nil, errors.New("nil_news_aggregator")
	}
	return &Handler{
		aggregator: a,
	}, nil
}

type errorResponse struct {
	Message string `json:"message,omitempty"`
	Code    string `json:"code,omitempty"`
}

func (h *Handler) GetArticles(rw http.ResponseWriter, r *http.Request) {
	userID := r.Header.Get("Authorization")
	if userID == "" {
		rw.WriteHeader(http.StatusUnauthorized)
		return
	}

	rw.Header().Set("Content-Type", "application/json")

	showRead, _ := strconv.ParseBool(r.URL.Query().Get("showread"))

	opts := news.QueryOptions{}

	categories := strings.Split(r.URL.Query().Get("categories"), ",")
	if categories[0] != "" {
		opts.Categories = categories
	}

	providers := strings.Split(r.URL.Query().Get("providers"), ",")
	if providers[0] != "" {
		opts.Providers = providers
	}
	if showRead {
		opts.ShowRead = showRead
	}

	articles, err := h.aggregator.GetArticles(r.Context(), userID, &opts)
	if err != nil {
		log.Println("get_articles: %w", err)
		rw.WriteHeader(http.StatusInternalServerError)

		err := errorResponse{
			Message: "something went wrong",
		}
		json.NewEncoder(rw).Encode(&err)
		return
	}

	resp := struct {
		Articles []*news.Item `json:"articles"`
	}{
		Articles: articles,
	}

	err = json.NewEncoder(rw).Encode(&resp)
	if err != nil {
		log.Println("get_articles: %w", err)
		rw.WriteHeader(http.StatusInternalServerError)

		err := errorResponse{
			Message: "something went wrong",
		}
		json.NewEncoder(rw).Encode(&err)
		return
	}
}

func (h *Handler) ReadArticleByID(rw http.ResponseWriter, r *http.Request) {
	userID := r.Header.Get("Authorization")
	if userID == "" {
		rw.WriteHeader(http.StatusUnauthorized)
		return
	}

	rw.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	articleID := vars["id"]
	if articleID == "" {
		rw.WriteHeader(http.StatusBadRequest)

		err := errorResponse{
			Message: "invalid input",
			Code:    "EMPTY_ARTICLE_ID",
		}
		json.NewEncoder(rw).Encode(&err)
		return
	}

	err := h.aggregator.MarkRead(r.Context(), userID, articleID)
	if err != nil {
		log.Printf("mark_read: %v", err)
		rw.WriteHeader(http.StatusInternalServerError)

		err := errorResponse{
			Message: "something went wrong",
		}
		json.NewEncoder(rw).Encode(&err)
		return
	}
}

func (h *Handler) ShareArticleByID(rw http.ResponseWriter, r *http.Request) {
	userID := r.Header.Get("Authorization")
	if userID == "" {
		rw.WriteHeader(http.StatusUnauthorized)
		return
	}
	rw.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	articleID := vars["id"]
	if articleID == "" {
		rw.WriteHeader(http.StatusBadRequest)

		err := errorResponse{
			Message: "invalid input",
			Code:    "EMPTY_ARTICLE_ID",
		}
		json.NewEncoder(rw).Encode(&err)
		return
	}

	link, err := h.aggregator.GenerateShareLink(r.Context(), userID, articleID)
	if err != nil {
		log.Printf("get_articles: %v", err)
		rw.WriteHeader(http.StatusInternalServerError)

		err := errorResponse{
			Message: "something went wrong",
		}
		json.NewEncoder(rw).Encode(&err)
		return
	}

	resp := struct {
		Link string `json:"link"`
	}{
		Link: link,
	}

	err = json.NewEncoder(rw).Encode(&resp)
	if err != nil {
		log.Println("get_articles: %w", err)
		rw.WriteHeader(http.StatusInternalServerError)

		err := errorResponse{
			Message: "something went wrong",
		}
		json.NewEncoder(rw).Encode(&err)
		return
	}
}

func (h *Handler) ApplyRoutes(m *mux.Router) {
	m.HandleFunc("/articles", h.GetArticles).Methods(http.MethodGet)
	m.HandleFunc("/articles/{id}/read", h.ReadArticleByID).Methods(http.MethodPost)
	m.HandleFunc("/articles/{id}/share", h.ShareArticleByID).Methods(http.MethodGet)
}

package transporthttp_test

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/mattnolf/ziglu/internal/news"
	"github.com/mattnolf/ziglu/internal/transport/transporthttp"
	mock_transporthttp "github.com/mattnolf/ziglu/mock/transport/transporthttp"
)

func Test_NewHandler(t *testing.T) {
	t.Run("return handler", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		assert.NotNil(t, handler)

	})
	t.Run("return error given nil aggregator", func(t *testing.T) {
		_, err := transporthttp.NewHandler(nil)
		assert.Error(t, err)
	})
}

func Test_HandlerGetArticles(t *testing.T) {
	accountID := "some-account-id"
	items := []*news.Item{
		{
			ID:    "1",
			Title: "some-title",
		},
		{
			ID:    "2",
			Title: "some-other-title",
		},
	}

	t.Run("return okay and news articles", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/articles", nil)
		r.Header.Set("Authorization", accountID)

		emptyOpts := news.QueryOptions{
			ShowRead: false,
		}

		ma.EXPECT().GetArticles(r.Context(), accountID, &emptyOpts).Return(items, nil)

		handler.GetArticles(rw, r)

		assert.Equal(t, http.StatusOK, rw.Result().StatusCode)

		type response struct {
			Articles []*news.Item `json:"articles"`
		}
		expected := response{
			Articles: items,
		}
		var actual response

		err = json.NewDecoder(rw.Result().Body).Decode(&actual)
		require.NoError(t, err)

		assert.Equal(t, expected, actual)
	})
	t.Run("return okay and category news articles", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/articles?categories=tech,general", nil)
		r.Header.Set("Authorization", accountID)

		emptyOpts := news.QueryOptions{
			Categories: []string{"tech", "general"},
			ShowRead:   false,
		}

		ma.EXPECT().GetArticles(r.Context(), accountID, &emptyOpts).Return(items, nil)

		handler.GetArticles(rw, r)

		assert.Equal(t, http.StatusOK, rw.Result().StatusCode)

		type response struct {
			Articles []*news.Item `json:"articles"`
		}
		expected := response{
			Articles: items,
		}
		var actual response

		err = json.NewDecoder(rw.Result().Body).Decode(&actual)
		require.NoError(t, err)

		assert.Equal(t, expected, actual)
	})
	t.Run("return okay and provider news articles", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/articles?providers=bbc,sky", nil)
		r.Header.Set("Authorization", accountID)

		emptyOpts := news.QueryOptions{
			Providers: []string{"bbc", "sky"},
			ShowRead:  false,
		}

		ma.EXPECT().GetArticles(r.Context(), accountID, &emptyOpts).Return(items, nil)

		handler.GetArticles(rw, r)

		assert.Equal(t, http.StatusOK, rw.Result().StatusCode)

		type response struct {
			Articles []*news.Item `json:"articles"`
		}
		expected := response{
			Articles: items,
		}
		var actual response

		err = json.NewDecoder(rw.Result().Body).Decode(&actual)
		require.NoError(t, err)

		assert.Equal(t, expected, actual)
	})
	t.Run("return okay and all read & unread news articles", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/articles?showread=true", nil)
		r.Header.Set("Authorization", accountID)

		emptyOpts := news.QueryOptions{
			ShowRead: true,
		}

		ma.EXPECT().GetArticles(r.Context(), accountID, &emptyOpts).Return(items, nil)

		handler.GetArticles(rw, r)

		assert.Equal(t, http.StatusOK, rw.Result().StatusCode)

		type response struct {
			Articles []*news.Item `json:"articles"`
		}
		expected := response{
			Articles: items,
		}
		var actual response

		err = json.NewDecoder(rw.Result().Body).Decode(&actual)
		require.NoError(t, err)

		assert.Equal(t, expected, actual)
	})
	t.Run("return unauthorized given invalid Authorization header", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/articles", nil)

		handler.GetArticles(rw, r)

		assert.Equal(t, http.StatusUnauthorized, rw.Result().StatusCode)
	})
	t.Run("return internal server error given aggregator error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/articles", nil)
		r.Header.Set("Authorization", accountID)

		emptyOpts := news.QueryOptions{
			ShowRead: false,
		}

		ma.EXPECT().GetArticles(r.Context(), accountID, &emptyOpts).Return(nil, errors.New("some-error"))

		handler.GetArticles(rw, r)

		assert.Equal(t, http.StatusInternalServerError, rw.Result().StatusCode)
	})
}

func Test_HandlerReadArticleByID(t *testing.T) {
	accountID := "some-account-id"
	articleID := "some-article-id"
	t.Run("return okay", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodPost, "/articles/some-id/read", nil)

		r = mux.SetURLVars(r, map[string]string{"id": articleID})
		r.Header.Set("Authorization", accountID)

		ma.EXPECT().MarkRead(r.Context(), accountID, articleID).Return(nil)

		handler.ReadArticleByID(rw, r)

		assert.Equal(t, http.StatusOK, rw.Result().StatusCode)

		assert.Empty(t, rw.Body)
	})
	t.Run("return unauthorized given invalid Authorization header", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodPost, "/articles/some-id/read", nil)

		r = mux.SetURLVars(r, map[string]string{"id": articleID})

		handler.ReadArticleByID(rw, r)

		assert.Equal(t, http.StatusUnauthorized, rw.Result().StatusCode)
	})
	t.Run("return internal server error given aggregator error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodPost, "/articles/some-id/read", nil)

		r = mux.SetURLVars(r, map[string]string{"id": articleID})
		r.Header.Set("Authorization", accountID)

		ma.EXPECT().MarkRead(r.Context(), accountID, articleID).Return(errors.New("some-error"))

		handler.ReadArticleByID(rw, r)

		assert.Equal(t, http.StatusInternalServerError, rw.Result().StatusCode)
	})
}

func Test_HandlerShareArticleByID(t *testing.T) {
	accountID := "some-account-id"
	articleID := "some-article-id"
	t.Run("return okay", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/articles/some-article-id/share", nil)

		r = mux.SetURLVars(r, map[string]string{"id": articleID})
		r.Header.Set("Authorization", accountID)

		ma.EXPECT().GenerateShareLink(r.Context(), accountID, articleID).Return("some-link", nil)

		handler.ShareArticleByID(rw, r)

		assert.Equal(t, http.StatusOK, rw.Result().StatusCode)

		type response struct {
			Link string `json:"link"`
		}
		expected := response{
			Link: "some-link",
		}
		var actual response

		err = json.NewDecoder(rw.Result().Body).Decode(&actual)
		require.NoError(t, err)

		assert.Equal(t, expected, actual)
	})
	t.Run("return unauthorized given invalid Authorization header", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodPost, "/articles/some-id/share", nil)

		r = mux.SetURLVars(r, map[string]string{"id": articleID})

		handler.ShareArticleByID(rw, r)

		assert.Equal(t, http.StatusUnauthorized, rw.Result().StatusCode)
	})
	t.Run("return bad request given bad article id", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/articles/share", nil)

		r.Header.Set("Authorization", accountID)

		handler.ShareArticleByID(rw, r)

		assert.Equal(t, http.StatusBadRequest, rw.Result().StatusCode)

		type response struct {
			Message string `json:"message,omitempty"`
			Code    string `json:"code,omitempty"`
		}
		expected := response{
			Message: "invalid input",
			Code:    "EMPTY_ARTICLE_ID",
		}
		var actual response

		err = json.NewDecoder(rw.Result().Body).Decode(&actual)
		require.NoError(t, err)

		assert.Equal(t, expected, actual)
	})
	t.Run("return internal server error given aggregator error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		ma := mock_transporthttp.NewMockAggregator(ctrl)

		handler, err := transporthttp.NewHandler(ma)
		require.NoError(t, err)

		rw := httptest.NewRecorder()
		r := httptest.NewRequest(http.MethodGet, "/articles/some-article-id/share", nil)

		r = mux.SetURLVars(r, map[string]string{"id": articleID})
		r.Header.Set("Authorization", accountID)

		ma.EXPECT().GenerateShareLink(r.Context(), accountID, articleID).Return("", errors.New("some-error"))

		handler.ShareArticleByID(rw, r)

		assert.Equal(t, http.StatusInternalServerError, rw.Result().StatusCode)

		type response struct {
			Message string `json:"message,omitempty"`
			Code    string `json:"code,omitempty"`
		}
		expected := response{
			Message: "something went wrong",
		}
		var actual response

		err = json.NewDecoder(rw.Result().Body).Decode(&actual)
		require.NoError(t, err)

		assert.Equal(t, expected, actual)
	})
}

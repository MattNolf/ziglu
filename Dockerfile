FROM golang:1.16 as build

RUN mkdir /app

ADD . /app/

WORKDIR /app

RUN go build -o app .

FROM gcr.io/distroless/base-debian10

COPY --from=build app/app /
COPY --from=build app/config.yaml /

ENTRYPOINT [ "/app" ]
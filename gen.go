package main

//go:generate mockgen -destination mock/transport/transporthttp/aggregator.gen.go -source internal/transport/transporthttp/handler.go Aggregator
//go:generate mockgen -destination mock/news/news.gen.go -source internal/news/news.go NewsFeed,Store,Generator

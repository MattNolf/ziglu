package main

import (
	"log"
	"net/http"
	"os"

	"gopkg.in/yaml.v2"

	"github.com/mattnolf/ziglu/internal/database/memorystore"
	"github.com/mattnolf/ziglu/internal/news"
	"github.com/mattnolf/ziglu/internal/source/bbctech"
	"github.com/mattnolf/ziglu/internal/source/skytech"
	"github.com/mattnolf/ziglu/internal/transport/transporthttp"
)

type config struct {
	Providers struct {
		SkyNews bool `yaml:"skynews"`
		BBCNews bool `yaml:"bbcnews"`
	}
}

func main() {
	b, err := os.ReadFile("config.yaml")
	if err != nil {
		log.Fatalf("read_config: %v", err)
	}

	var cfg config
	err = yaml.Unmarshal(b, &cfg)
	if err != nil {
		log.Fatalf("unmarshal_config: %v", err)
	}

	c := http.Client{}

	var feeds []news.NewsFeed

	if cfg.Providers.SkyNews {
		skyClient, err := skytech.NewClient("http://feeds.skynews.com/feeds/rss/technology.xml", &c)
		if err != nil {
			log.Fatalf("create_sky_client: %v", err)
		}
		feeds = append(feeds, skyClient)
	}
	if cfg.Providers.BBCNews {
		bbcClient, err := bbctech.NewClient("http://feeds.bbci.co.uk/news/uk/rss.xml", &c)
		if err != nil {
			log.Fatalf("create_bbc_client: %v", err)
		}
		feeds = append(feeds, bbcClient)
	}

	mr := memorystore.NewClient()

	news, err := news.New(feeds, mr, news.UUIDGenerator{})
	if err != nil {
		log.Fatalf("create_news_aggregator: %v", err)
	}

	h, err := transporthttp.NewHandler(news)
	if err != nil {
		log.Fatalf("create_http_handler: %v", err)
	}

	if err := transporthttp.ListenAndServe(":8080", h); err != nil {
		log.Fatal(err)
	}
}

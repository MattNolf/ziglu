# News API

An application to aggregate and return news feed items from providers.

---

### Assumptions
- That eventually we may need to support multiple sources simultanously.

- News feed locations means keeping track of which articles a user has read.

- Using an in-memory storage solution is acceptable for this test case.
---

### Running Locally
The application is built using Go 1.16, install go and verify the installation with `go version`

Once your environment is setup, simply `go run main.go`. This will install any missing dependancies and run the server on port `8080`.

The `config.yaml` provides service configuration - which currently is the sources to retrieve from. BBC is not yet implemented, and so will cause the service to exit with an error indicating so.

#### Docker 
If you wish to run the application using Docker, you can pull and run the following image:
```
docker run --rm --name news-api -p 8080:8080 mattnolf/ziglu:latest
```
The image is a prebuilt version of that available in the `Dockerfile` with sky enabled.

### Running Tests
To run the application tests, run the following:
```
go test -v -coverprofile cover.out --count=1 ./...
```

Some integration tests are also included to sky technology news, which can be ran with
```
go test -v -coverprofile cover.out --tags=integration --count=1 ./...
```

---

### API

Load news articles
```
GET /articles

Response:
200 Okay
{
    "articles": [
        {
            "id": string
            "title": string
            "description": string
            "url": string
            "imageURL": string
            "category": string
            "provider": string
            "timestamp": timestamp
        },

    ]
}

params:
- category: Comma seperated list of news article (e.g. technology/food)
- provider: Comma seperated list of news providers (e.g. bbc/sky)
- showread: Whether to show articles already read by the user
```

```
curl -H "Authorization: user1" localhost:8080/articles 
```


Generate a shareable link to the given news article.
```
GET /articles/{id}/share

200 Okay
{
    "link": "some-link"
}
```

```
curl -H "Authorization: user1" localhost:8080/articles/some-id/share
```


Mark the given news article as read to remove it from being returned in future requests.
```
PUT /articles/{id}/read

200 Okay
```

```
curl -X PUT -H "Authorization: user1" localhost:8080/articles/some-id/share
```
---

### Decisions / Future Iterations

**Microservices**

The task description mentions adopting a microservice architecture. This is a single application / deployment, that receives requests, stores users read receipts and interfaces with news providers. 

The diagram below suggests a potential next step in evolving to support more complex provider usages, or where other use cases may also require such an interface.

![Adapters](docs/fig1.png "Microservice adapters for news providers")


**Monitoring**

There is a lack of monitoring configured to help manage the system running in production. A next iteration would include adding metrics, better structured logging, and instrumented http clients that call out to the news providers.

**Error Handling**

Error handling is not very granular with the current implementation - which limits useful API error feedback.
A next iteration would involve creating more sentinal errors which can be used to better derive response codes and messages.

**API**

A next iteration should support some method of pagination, to help create a feed that can be scrolled when needed, but without loading all data up front. This may be possible with the addition of some kind of `pos` response field to indicate position and next data to collect. 